# Evobot hardware  repository#


This repository contains the design files for the Evobot robot, which is part of the [Evobliss project](http://evobliss-project.eu/). 

### Mechanics design ###

The design is made in Solidworks, but you can also find the files for the printed parts (*.STL) or for the cut parts (*.PDF)
 
### Electronics design ###

The design is made with Design Spark, but you can also find the gerber files.
