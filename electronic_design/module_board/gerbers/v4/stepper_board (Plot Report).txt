Plot Report
-----------

Report Written:     Tuesday, January 30, 2018
Project Path:       C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\stepper_board.prj
Design Path:        C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\stepper_board.pcb
Design Title:       
Created:            13/11/2014 10:46:22
Last Saved:         30/01/2018 19:03:05
Editing Time:       16458 min
Units:              mm (precision 2)


Device Settings
===============


Gerber Settings
===============

    Leading zero suppression.
    G01 assumed throughout.
    Line termination <*> <CR> <LF>.
    3.5 format absolute inches.
    Format commands defined in Gerber file.
    Aperture table defined in Gerber file.
    Hardware arcs allowed.


Drill Settings
==============

    Excellon Format 2    Format 2.3 absolute in inches.
    No zero suppression.


Plots Output
============

    Plot Job: "C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\stepper_board.mop"


Gerber Output
=============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - Top Silkscreen.gbr

    Layers:
    ------------------
    Top Silkscreen
    Board Outlines

    D-Code Shape  (thou)
    -----------------------
    D10    Round 5.00
    D12    Round 6.00
    D13    Round 3.00
    D15    Round 10.00
    D78    Round 1.00
    D137   Round 6.69

Gerber Output
=============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - Top Copper.gbr

    Layers:
    ------------------
    Top Copper
    Board Outlines

    D-Code Shape  (thou)
    ------------------------------------------------
    D10    Round 5.00
    D24    Rectangle 55.00 X 60.00
    D28    Round 48.00
    D29    Round 15.00
    D78    Round 1.00
    D87    Rectangle 55.00 X 60.00  Rotation 90
    D91    Round 50.00
    D108   Rectangle 31.00 X 149.00  Rotation 90
    D109   Rectangle 15.35 X 51.97
    D110   Rectangle 129.92 X 236.22  Rotation 90
    D111   Rectangle 50.00 X 125.00
    D112   Rectangle 59.06 X 196.85
    D113   Rectangle 20.00 X 40.00  Rotation 90
    D114   Rectangle 20.00 X 47.71  Rotation 90
    D115   Rectangle 36.00 X 57.00  Rotation 90
    D116   Rectangle 36.00 X 63.00
    D117   Rectangle 36.00 X 57.00
    D118   Round 32.00
    D119   Round 8.00
    D132   Round 25.00

Gerber Output
=============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - Top Copper (Paste).gbr

    Layers:
    --------------------
    Top Solder Paste
    Board Outlines

    D-Code Shape  (thou)
    ------------------------------------------------
    D76    Rectangle 51.00 X 56.00
    D78    Round 1.00
    D94    Rectangle 51.00 X 56.00  Rotation 90
    D96    Rectangle 27.00 X 145.00  Rotation 90
    D97    Rectangle 11.35 X 47.97
    D98    Rectangle 125.92 X 232.22  Rotation 90
    D99    Rectangle 46.00 X 121.00
    D100   Rectangle 55.06 X 192.85
    D101   Rectangle 16.00 X 36.00  Rotation 90
    D102   Rectangle 16.00 X 43.71  Rotation 90
    D103   Rectangle 32.00 X 53.00  Rotation 90
    D104   Rectangle 32.00 X 59.00
    D105   Rectangle 32.00 X 53.00

Gerber Output
=============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - Top Solder Mask.gbr

    Layers:
    -------------------
    Top Solder Mask
    Board Outlines

    D-Code Shape  (thou)
    ------------------------------------------------
    D78    Round 1.00
    D120   Rectangle 37.00 X 155.00  Rotation 90
    D121   Rectangle 21.35 X 57.97
    D122   Rectangle 135.92 X 242.22  Rotation 90
    D123   Rectangle 56.00 X 131.00
    D124   Rectangle 65.06 X 202.85
    D125   Rectangle 26.00 X 46.00  Rotation 90
    D126   Rectangle 26.00 X 53.71  Rotation 90
    D127   Rectangle 42.00 X 63.00  Rotation 90
    D128   Rectangle 42.00 X 69.00
    D129   Rectangle 61.00 X 66.00
    D130   Rectangle 42.00 X 63.00
    D131   Rectangle 61.00 X 66.00  Rotation 90

Gerber Output
=============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - Bottom Copper.gbr

    Layers:
    ------------------
    Bottom Copper
    Board Outlines

    D-Code Shape  (thou)
    -----------------------
    D10    Round 5.00
    D28    Round 48.00
    D29    Round 15.00
    D78    Round 1.00
    D91    Round 50.00
    D118   Round 32.00
    D119   Round 8.00
    D132   Round 25.00

Gerber Output
=============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - Bottom Solder Mask.gbr

    Layers:
    ----------------------
    Bottom Solder Mask
    Board Outlines

    D-Code Shape  (thou)
    -----------------------
    D78    Round 1.00

NC Drill Output
===============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - Drill Data - Through Hole.drl

    Output:
    -----------------------------
    Plated Round Drill Holes
    Unplated Round Drill Holes
    Plated Slots
    Unplated Slots
    Plated Board Outlines
    Unplated Board Outlines

    Tool  Size      Count   ID  (Plated/Unplated)
    ------------------------------------------------------
    T001  00.016 in 33
    T002  00.024 in 19
    T003  00.025 in 5
    ------------------------------------------------------
    Total           57
    ------------------------------------------------------


Gerber Output
=============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - Drill Ident Drawing - Through Hole.gbr

    Layers:
    ------------------
    [Through Hole]


Drill Ident Drawing
===================

    Drill Plated Size Shape ID
    -----------------------------------
    16    Y      100  Round A
    24    Y      100  Round B

    D-Code Shape  (thou)
    -----------------------
    D10    Round 5.00
    D79    Round 100.00

Gerber Output
=============

C:\fai\documents\git\EVOBLISS-HARDWARE\electronic_design\module_board\gerbers\v4\stepper_board - BoardOutline.gbr

    Layers:
    ------------------
    Board Outlines

    D-Code Shape  (thou)
    -----------------------
    D78    Round 1.00

End Of Report.
